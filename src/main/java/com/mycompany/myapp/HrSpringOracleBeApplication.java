package com.mycompany.myapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HrSpringOracleBeApplication {

	public static void main(String[] args) {
		SpringApplication.run(HrSpringOracleBeApplication.class, args);
	}
}
