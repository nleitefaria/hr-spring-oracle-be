package com.mycompany.myapp.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.mycompany.myapp.model.Countries;

@Repository
public interface CountriesRepository  extends JpaRepository<Countries, String>{
	
	@Query(value = "SELECT c FROM Countries c LEFT JOIN FETCH c.regions r where c.countryId = :id")   
    Countries findById(@Param("id") String id);
	
	@Query(value = "SELECT c FROM Countries c LEFT JOIN FETCH c.regions r")   
    List<Countries> findAllCountries();

}
