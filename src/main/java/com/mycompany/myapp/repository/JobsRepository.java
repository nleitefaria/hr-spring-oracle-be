package com.mycompany.myapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mycompany.myapp.model.Jobs;

public interface JobsRepository extends JpaRepository<Jobs, String>{

}
