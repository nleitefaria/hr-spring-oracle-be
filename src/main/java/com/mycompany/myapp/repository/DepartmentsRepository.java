package com.mycompany.myapp.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.mycompany.myapp.model.Departments;

public interface DepartmentsRepository  extends JpaRepository<Departments, Short>{
	
	@Query(value = "SELECT d FROM Departments d LEFT JOIN FETCH d.locations l LEFT JOIN FETCH d.employees e where d.departmentId = :id")   
	Departments findById(@Param("id") Short id);
	
	@Query(value = "SELECT d FROM Departments d LEFT JOIN FETCH d.locations l LEFT JOIN FETCH d.employees e")   
    List<Departments> findAllDepartments();

}
