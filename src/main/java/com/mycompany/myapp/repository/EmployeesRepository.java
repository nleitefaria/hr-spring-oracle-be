package com.mycompany.myapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mycompany.myapp.model.Employees;

public interface EmployeesRepository extends JpaRepository<Employees, Integer>{

}
