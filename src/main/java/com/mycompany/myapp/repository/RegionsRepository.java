package com.mycompany.myapp.repository;

import java.math.BigDecimal;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.mycompany.myapp.model.Regions;

@Repository
public interface RegionsRepository extends JpaRepository<Regions, BigDecimal> {

}
