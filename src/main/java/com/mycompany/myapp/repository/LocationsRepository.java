package com.mycompany.myapp.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import com.mycompany.myapp.model.Locations;

@Repository
public interface LocationsRepository extends JpaRepository<Locations, Short>{
	
	@Query(value = "SELECT l FROM Locations l LEFT JOIN FETCH l.countries c where l.locationId = :id")   
	Locations findById(@Param("id") Short id);
	
	@Query(value = "SELECT l FROM Locations l LEFT JOIN FETCH l.countries c")   
    List<Locations> findAllLocations();

}
