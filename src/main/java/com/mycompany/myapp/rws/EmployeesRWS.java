package com.mycompany.myapp.rws;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.mycompany.myapp.model.Employees;
import com.mycompany.myapp.service.EmployeesService;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class EmployeesRWS 
{
	private static final Logger logger = LoggerFactory.getLogger(EmployeesRWS.class);
	
	@Autowired
	EmployeesService employeesService;
	
	@RequestMapping(value = "/employees/{id}", method = RequestMethod.GET)
	public ResponseEntity<Employees> findOne(@PathVariable Integer id)
	{
		logger.info("Listing employees with id: " + id);
		return new ResponseEntity<Employees>(employeesService.findById(id), HttpStatus.OK);
	}
	
	@RequestMapping(value = "/employees", method = RequestMethod.GET)
	public ResponseEntity<List<Employees>> findAll()
	{
		logger.info("Listing all employees");
		return new ResponseEntity<List<Employees>>(employeesService.findAll(), HttpStatus.OK);
	}

}
