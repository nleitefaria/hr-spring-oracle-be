package com.mycompany.myapp.rws;

import java.math.BigDecimal;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.mycompany.myapp.domain.RegionsDTO;
import com.mycompany.myapp.service.RegionsService;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class RegionsRWS 
{
	private static final Logger logger = LoggerFactory.getLogger(RegionsRWS.class);
	
	@Autowired
	RegionsService regionsService;
	
	@RequestMapping(value = "/regions/{id}", method = RequestMethod.GET)
	public ResponseEntity<RegionsDTO> findOne(@PathVariable String id)
	{
		logger.info("Listing regions with id: " + id);
		return new ResponseEntity<RegionsDTO>(regionsService.findById(id), HttpStatus.OK);
	}
	
	@RequestMapping(value = "/regions", method = RequestMethod.GET)
	public ResponseEntity<List<RegionsDTO>> findAll()
	{
		logger.info("Listing all regions");
		return new ResponseEntity<List<RegionsDTO>>(regionsService.findAll(), HttpStatus.OK);
	}
	
	@RequestMapping(value = "/regions", method = RequestMethod.POST)
	public ResponseEntity<RegionsDTO> create(@RequestBody RegionsDTO regionsDTO)
	{
		logger.info("Creating region");
		regionsService.save(regionsDTO);
		return new ResponseEntity<RegionsDTO>(regionsDTO, HttpStatus.CREATED);
	}
	
	@RequestMapping(value = "/regions/{id}", method = RequestMethod.PUT)
	public ResponseEntity<RegionsDTO> update(@PathVariable("id") BigDecimal id, @RequestBody RegionsDTO regionsDTO) 
	{
		logger.info("Updating region");
		regionsService.update(id, regionsDTO);		 
		return new ResponseEntity<RegionsDTO>(regionsDTO, HttpStatus.OK);		 
	}
	
	@RequestMapping(value = "/regions/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<RegionsDTO> deleteUser(@PathVariable("id") BigDecimal id) 
	{
		logger.info("Deleting regions with id: " + id);
		regionsService.delete(id);
        return new ResponseEntity<RegionsDTO>(HttpStatus.NO_CONTENT);
    }
}
