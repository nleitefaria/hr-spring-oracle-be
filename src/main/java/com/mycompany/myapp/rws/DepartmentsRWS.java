package com.mycompany.myapp.rws;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.mycompany.myapp.domain.DepartmentsDTO;
import com.mycompany.myapp.service.DepartmentsService;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class DepartmentsRWS 
{
	private static final Logger logger = LoggerFactory.getLogger(DepartmentsRWS.class);
	
	@Autowired
	DepartmentsService departmentsService;
	
	@RequestMapping(value = "/departments/{id}", method = RequestMethod.GET)
	public ResponseEntity<DepartmentsDTO> findOne(@PathVariable String id)
	{
		logger.info("Listing departments with id: " + id);
		return new ResponseEntity<DepartmentsDTO>(departmentsService.findById(id), HttpStatus.OK);
	}
	
	@RequestMapping(value = "/departments", method = RequestMethod.GET)
	public ResponseEntity<List<DepartmentsDTO>> findAll()
	{
		logger.info("Listing all departments");
		return new ResponseEntity<List<DepartmentsDTO>>(departmentsService.findAll(), HttpStatus.OK);
	}
	
	@RequestMapping(value = "/departments", method = RequestMethod.POST)
	public ResponseEntity<DepartmentsDTO> create(@RequestBody DepartmentsDTO departmentsDTO)
	{
		logger.info("Creating department");
		departmentsService.save(departmentsDTO);
		return new ResponseEntity<DepartmentsDTO>(departmentsDTO, HttpStatus.CREATED);
	}
	
	@RequestMapping(value = "/departments/{id}", method = RequestMethod.PUT)
	public ResponseEntity<DepartmentsDTO> update(@PathVariable("id") Short id, @RequestBody DepartmentsDTO departmentsDTO) 
	{
		logger.info("Updating department");
		departmentsService.update(id, departmentsDTO);		 
		return new ResponseEntity<DepartmentsDTO>(departmentsDTO, HttpStatus.OK);		 
	}
	
	@RequestMapping(value = "/departments/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<DepartmentsDTO> deleteUser(@PathVariable("id") Short id) 
	{
		logger.info("Deleting departments with id: " + id);
		departmentsService.delete(id);
        return new ResponseEntity<DepartmentsDTO>(HttpStatus.NO_CONTENT);
    }

}
