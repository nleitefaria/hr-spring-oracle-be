package com.mycompany.myapp.rws;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.mycompany.myapp.domain.JobsDTO;
import com.mycompany.myapp.service.JobsService;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class JobsRWS
{	
	private static final Logger logger = LoggerFactory.getLogger(JobsRWS.class);
	
	@Autowired
	JobsService jobsService;
	
	@RequestMapping(value = "/jobs/{id}", method = RequestMethod.GET)
	public ResponseEntity<JobsDTO> findOne(@PathVariable String id)
	{
		logger.info("Listing employees with id: " + id);
		return new ResponseEntity<JobsDTO>(jobsService.findById(id), HttpStatus.OK);
	}
	
	@RequestMapping(value = "/jobs", method = RequestMethod.GET)
	public ResponseEntity<List<JobsDTO>> findAll() 
	{
		logger.info("Listing all jobs");
		return new ResponseEntity<List<JobsDTO>>(jobsService.findAll(), HttpStatus.OK);
	}
	
	@RequestMapping(value = "/jobs", method = RequestMethod.POST)
	public ResponseEntity<JobsDTO> create(@RequestBody JobsDTO jobsDTO)
	{
		logger.info("Creating jobs");
		jobsService.save(jobsDTO);
		return new ResponseEntity<JobsDTO>(jobsDTO, HttpStatus.CREATED);
	}

}
