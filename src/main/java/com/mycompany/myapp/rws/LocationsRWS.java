package com.mycompany.myapp.rws;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.mycompany.myapp.domain.LocationsDTO;
import com.mycompany.myapp.service.LocationsService;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class LocationsRWS 
{
	private static final Logger logger = LoggerFactory.getLogger(LocationsRWS.class);
	
	@Autowired
	LocationsService locationsService;
	
	@RequestMapping(value = "/locations/{id}", method = RequestMethod.GET)
	public ResponseEntity<LocationsDTO> findOne(@PathVariable String id)
	{
		logger.info("Listing locations with id: " + id);
		return new ResponseEntity<LocationsDTO>(locationsService.findById(id), HttpStatus.OK);
	}
	
	@RequestMapping(value = "/locations", method = RequestMethod.GET)
	public ResponseEntity<List<LocationsDTO>> findAll()
	{
		logger.info("Listing all locations");
		return new ResponseEntity<List<LocationsDTO>>(locationsService.findAll(), HttpStatus.OK);
	}
	
//	@RequestMapping(value = "/regions", method = RequestMethod.POST)
//	public ResponseEntity<RegionsDTO> create(@RequestBody RegionsDTO regionsDTO)
//	{
//		logger.info("Creating region");
//		regionsService.save(regionsDTO);
//		return new ResponseEntity<RegionsDTO>(regionsDTO, HttpStatus.CREATED);
//	}

}
