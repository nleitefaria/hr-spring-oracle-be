package com.mycompany.myapp.rws;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.mycompany.myapp.domain.CountriesDTO;
import com.mycompany.myapp.service.CountriesService;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class CountriesRWS 
{
	private static final Logger logger = LoggerFactory.getLogger(CountriesRWS.class);
	
	@Autowired
	CountriesService countriesService;
	
	@RequestMapping(value = "/countries/{id}", method = RequestMethod.GET)
	public ResponseEntity<CountriesDTO> findOne(@PathVariable String id)
	{
		logger.info("Listing country with id: " + id);
		return new ResponseEntity<CountriesDTO>(countriesService.findById(id), HttpStatus.OK);
	}
	
	@RequestMapping(value = "/countries", method = RequestMethod.GET)
	public ResponseEntity<List<CountriesDTO>> findAll()
	{
		logger.info("Listing all countries");
		return new ResponseEntity<List<CountriesDTO>>(countriesService.findAllCountries(), HttpStatus.OK);
	}
	
	@RequestMapping(value = "/countries", method = RequestMethod.POST)
	public ResponseEntity<CountriesDTO> create(@RequestBody CountriesDTO countriesDTO)
	{
		logger.info("Creating country");
		countriesService.save(countriesDTO);
		return new ResponseEntity<CountriesDTO>(countriesDTO, HttpStatus.CREATED);
	}
	
	 @RequestMapping(value = "/countries/{id}", method = RequestMethod.PUT)
	 public ResponseEntity<CountriesDTO> update(@PathVariable("id") String id, @RequestBody CountriesDTO countriesDTO) 
	 {
		logger.info("Updating country");
		countriesService.update(id, countriesDTO);		 
		return new ResponseEntity<CountriesDTO>(countriesDTO, HttpStatus.OK);		 
	 }
	 
	 @RequestMapping(value = "/countries/{id}", method = RequestMethod.DELETE)
	 public ResponseEntity<CountriesDTO> deleteUser(@PathVariable("id") String id) 
	 {
		logger.info("Deleting countries with id: " + id);
		countriesService.delete(id);
	    return new ResponseEntity<CountriesDTO>(HttpStatus.NO_CONTENT);
	 }

}
