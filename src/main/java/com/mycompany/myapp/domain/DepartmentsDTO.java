package com.mycompany.myapp.domain;

public class DepartmentsDTO 
{
	private short departmentId;
	private String locationId;
	private String employeeId;
	private String departmentName;

	public DepartmentsDTO() {
	}

	public DepartmentsDTO(short departmentId, String locationId, String employeeId, String departmentName) 
	{
		this.departmentId = departmentId;
		this.locationId = locationId;
		this.employeeId = employeeId;
		this.departmentName = departmentName;
	}

	public short getDepartmentId() {
		return departmentId;
	}

	public void setDepartmentId(short departmentId) {
		this.departmentId = departmentId;
	}

	public String getLocationId() {
		return locationId;
	}

	public void setLocationId(String locationId) {
		this.locationId = locationId;
	}

	public String getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(String employeeId) {
		this.employeeId = employeeId;
	}

	public String getDepartmentName() {
		return departmentName;
	}

	public void setDepartmentName(String departmentName) {
		this.departmentName = departmentName;
	}
}
