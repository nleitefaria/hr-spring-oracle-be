package com.mycompany.myapp.domain;

import java.math.BigDecimal;

import com.mycompany.myapp.model.Countries;

public class CountriesDTO 
{	
	private String countryId;
	private String countryName;
    private BigDecimal regionsId;
     
	public CountriesDTO() {
		
	}
	public CountriesDTO(String countryId, String countryName, BigDecimal regionsId) 
	{
		this.countryId = countryId;
		this.countryName = countryName;
		this.regionsId = regionsId;
	}
	public String getCountryId() {
		return countryId;
	}
	public void setCountryId(String countryId) {
		this.countryId = countryId;
	}
	public String getCountryName() {
		return countryName;
	}
	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}
	public BigDecimal getRegionsId() {
		return regionsId;
	}
	public void setRegionsId(BigDecimal regionsId) {
		this.regionsId = regionsId;
	}
	
	public CountriesDTO toCountriesDTO(Countries countries)
	{
		return new CountriesDTO(countries.getCountryId(), countries.getCountryName(), countries.getRegions().getRegionId());	
	}
}
