package com.mycompany.myapp.domain;

import java.math.BigDecimal;

public class RegionsDTO 
{	
	private BigDecimal regionId;
    private String regionName;
     
	public RegionsDTO() {
		
	}
	public RegionsDTO(BigDecimal regionId, String regionName) {
		this.regionId = regionId;
		this.regionName = regionName;
	}
	public BigDecimal getRegionId() {
		return regionId;
	}
	public void setRegionId(BigDecimal regionId) {
		this.regionId = regionId;
	}
	public String getRegionName() {
		return regionName;
	}
	public void setRegionName(String regionName) {
		this.regionName = regionName;
	}
}
