package com.mycompany.myapp.service;

import java.util.List;

import com.mycompany.myapp.domain.CountriesDTO;

public interface CountriesService
{
	CountriesDTO findById(String id);
	List<CountriesDTO> findAllCountries();
	void save(CountriesDTO countriesDTO);
	void update(String id, CountriesDTO countriesDTO);
	void delete(String id);
	void deleteAll();
}
