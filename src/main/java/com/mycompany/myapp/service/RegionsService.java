package com.mycompany.myapp.service;

import java.math.BigDecimal;
import java.util.List;

import com.mycompany.myapp.domain.RegionsDTO;

public interface RegionsService 
{
	RegionsDTO findById(String id);
	List<RegionsDTO> findAll();
	void save(RegionsDTO regionsDTO);
	void update(BigDecimal id, RegionsDTO regionsDTO);
	void delete(BigDecimal id);
	void deleteAll();
}
