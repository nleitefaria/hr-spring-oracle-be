package com.mycompany.myapp.service;

import java.util.List;

import com.mycompany.myapp.domain.DepartmentsDTO;


public interface DepartmentsService 
{	
	DepartmentsDTO findById(String id);
	List<DepartmentsDTO> findAll();
	void save(DepartmentsDTO departmentsDTO);
	void update(short id, DepartmentsDTO departmentsDTO) ;
	void delete(short id);
	void deleteAll();
}
