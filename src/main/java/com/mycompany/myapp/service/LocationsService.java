package com.mycompany.myapp.service;

import java.util.List;

import com.mycompany.myapp.domain.LocationsDTO;

public interface LocationsService {
	
	LocationsDTO findById(String id);
	List<LocationsDTO> findAll();
	void save(LocationsDTO locationsDTO);
	void deleteAll();

}
