package com.mycompany.myapp.service;

import java.util.List;

import com.mycompany.myapp.model.Employees;

public interface EmployeesService 
{	
	Employees findById(int id);
	List<Employees> findAll();
	void save(Employees employees);
	void deleteAll();
}
