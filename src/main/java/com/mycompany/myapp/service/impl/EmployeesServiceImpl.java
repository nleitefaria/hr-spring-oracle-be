package com.mycompany.myapp.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mycompany.myapp.model.Employees;
import com.mycompany.myapp.repository.EmployeesRepository;
import com.mycompany.myapp.service.EmployeesService;

@Service
public class EmployeesServiceImpl implements EmployeesService{
	
	@Resource
	private EmployeesRepository employeesRepository;
	
	@Transactional
	public Employees findById(int id) {
		return employeesRepository.findOne(id);
	}
	
	@Transactional
	public List<Employees> findAll() {
		return employeesRepository.findAll();
	}
	
	@Transactional
	public void save(Employees employees) {
		employeesRepository.save(employees);
	}
	
	@Transactional
	public void deleteAll() {
		employeesRepository.deleteAll();
	}

}
