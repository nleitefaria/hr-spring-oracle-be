package com.mycompany.myapp.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mycompany.myapp.domain.CountriesDTO;
import com.mycompany.myapp.model.Countries;
import com.mycompany.myapp.model.Regions;
import com.mycompany.myapp.repository.CountriesRepository;
import com.mycompany.myapp.repository.RegionsRepository;
import com.mycompany.myapp.service.CountriesService;

@Service
public class CountriesServiceImpl implements CountriesService 
{
	@Resource
	private CountriesRepository countriesRepository;
	
	@Resource
	private RegionsRepository regionsRepository;
	
	@Transactional
	public CountriesDTO findById(String id) 
	{	
		Countries countries = countriesRepository.findById(id);
		return new CountriesDTO(countries.getCountryId(), countries.getCountryName(), countries.getRegions().getRegionId()); 
	}
	
	@Transactional
	public List<CountriesDTO> findAllCountries() 
	{		
		List<CountriesDTO> ret = new ArrayList<CountriesDTO>();
		for(Countries countries : countriesRepository.findAllCountries())
		{
			ret.add(new CountriesDTO(countries.getCountryId(), countries.getCountryName(), countries.getRegions().getRegionId()));
		}	
		return ret;
	}
	
	@Transactional
	public void save(CountriesDTO countriesDTO)
	{	
		Countries countries = new Countries(countriesDTO.getCountryId());
		countries.setCountryName(countriesDTO.getCountryName());	
		if(countriesDTO.getRegionsId() != null)
		{
			Regions regions = regionsRepository.findOne(countriesDTO.getRegionsId());	
			if(regions != null)
			{
				countries.setRegions(regions);
			}
		}		
		countriesRepository.save(countries);
	}
	
	@Transactional
	public void update(String id, CountriesDTO countriesDTO)
	{	
		Countries countries = countriesRepository.findById(id);
		if(countriesDTO.getCountryName() != null)
		{
			countries.setCountryName(countriesDTO.getCountryName());			
		}	
		if(countriesDTO.getRegionsId() != null)
		{
			Regions regions = regionsRepository.findOne(countriesDTO.getRegionsId());	
			if(regions != null)
			{
				countries.setRegions(regions);
			}
		}		
		countriesRepository.save(countries);
	}
	
	@Transactional
	public void delete(String id)
	{	
		Countries countries = countriesRepository.findOne(id);
		countriesRepository.delete(countries);
	}
	
	@Transactional
	public void deleteAll() {
		countriesRepository.deleteAll();
	}

}
