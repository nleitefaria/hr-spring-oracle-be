package com.mycompany.myapp.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mycompany.myapp.domain.DepartmentsDTO;
import com.mycompany.myapp.model.Departments;
import com.mycompany.myapp.model.Employees;
import com.mycompany.myapp.model.Locations;
import com.mycompany.myapp.repository.DepartmentsRepository;
import com.mycompany.myapp.repository.EmployeesRepository;
import com.mycompany.myapp.repository.LocationsRepository;
import com.mycompany.myapp.service.DepartmentsService;

@Service
public class DepartmentsServiceImpl implements DepartmentsService 
{
	@Resource
	private DepartmentsRepository departmentsRepository;
	
	@Resource
	private LocationsRepository locationsRepository;
	
	@Resource
	private EmployeesRepository employeesRepository;
	
	@Transactional
	public DepartmentsDTO findById(String id)
	{	
		Departments departments = departmentsRepository.findById(Short.valueOf(id));
		
		String locationId = null;
		String employeeId = null;
		if(departments.getLocations()!= null)
		{
			locationId = String.valueOf(departments.getLocations().getLocationId());
		}
		
		if(departments.getEmployees()!= null)
		{
			String.valueOf(departments.getEmployees().getEmployeeId());				
		}
		
		DepartmentsDTO departementsDTO = new DepartmentsDTO(departments.getDepartmentId(), locationId, employeeId, departments.getDepartmentName());			
		return departementsDTO;	
	}
	
	@Transactional
	public List<DepartmentsDTO> findAll()
	{		
		List<DepartmentsDTO> ret = new ArrayList<DepartmentsDTO>(); 	
		String locationId = null;
		String employeeId = null;
		for(Departments departments : departmentsRepository.findAllDepartments())
		{			
			if(departments.getLocations()!= null)
			{
				locationId = String.valueOf(departments.getLocations().getLocationId());
			}
			
			if(departments.getEmployees()!= null)
			{
				String.valueOf(departments.getEmployees().getEmployeeId());				
			}
			
			ret.add(new DepartmentsDTO(departments.getDepartmentId(), locationId, employeeId, departments.getDepartmentName()));				
		}	
		return ret;
	}
	
	@Transactional
	public void save(DepartmentsDTO departmentsDTO) 
	{	
		Departments departments = new Departments(departmentsDTO.getDepartmentId(), departmentsDTO.getDepartmentName());
		
		if(departmentsDTO.getLocationId() != null)
		{
			Locations locations = locationsRepository.findById(Short.parseShort(departmentsDTO.getLocationId()));
			departments.setLocations(locations);
		}
		
		if(departmentsDTO.getEmployeeId() != null)
		{
			Employees employees = employeesRepository.findOne(Integer.parseInt(departmentsDTO.getLocationId()));
			departments.setEmployees(employees);		
		}
		
		departmentsRepository.save(departments);
	}
	
	
	@Transactional
	public void update(short id, DepartmentsDTO departmentsDTO) 
	{	
		Departments departments = departmentsRepository.findById(id);
		
		if(departmentsDTO.getDepartmentName() != null)
		{
			departments.setDepartmentName(departmentsDTO.getDepartmentName());
		}
		
		if(departmentsDTO.getLocationId() != null)
		{
			Locations locations = locationsRepository.findById(Short.parseShort(departmentsDTO.getLocationId()));
			departments.setLocations(locations);
		}
		
		if(departmentsDTO.getEmployeeId() != null)
		{
			Employees employees = employeesRepository.findOne(Integer.parseInt(departmentsDTO.getLocationId()));
			departments.setEmployees(employees);		
		}
		
		departmentsRepository.save(departments);
	}
	
	@Transactional
	public void delete(short id)
	{	
		Departments departments = departmentsRepository.findById(id);
		departmentsRepository.delete(departments);
	}
	
	
	@Transactional
	public void deleteAll() 
	{
		departmentsRepository.deleteAll();
	}

}
