package com.mycompany.myapp.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mycompany.myapp.domain.LocationsDTO;
import com.mycompany.myapp.model.Countries;
import com.mycompany.myapp.model.Locations;
import com.mycompany.myapp.repository.CountriesRepository;
import com.mycompany.myapp.repository.LocationsRepository;
import com.mycompany.myapp.service.LocationsService;

@Service
public class LocationsServiceImpl implements LocationsService 
{
	@Resource
	private LocationsRepository locationsRepository;
	
	@Resource
	private CountriesRepository countriesRepository;
	
	
	@Transactional
	public LocationsDTO findById(String id) 
	{
		Locations locations = locationsRepository.findById(new Short(id));
		return new LocationsDTO(locations.getLocationId(), locations.getCountries().getCountryId(), locations.getStreetAddress(), locations.getPostalCode(), locations.getCity(), locations.getStateProvince());
	}
	
	@Transactional
	public List<LocationsDTO> findAll() 
	{
		List<LocationsDTO> ret = new ArrayList<LocationsDTO>();	
		for(Locations locations : locationsRepository.findAllLocations())
		{
			ret.add(new LocationsDTO(locations.getLocationId(), locations.getCountries().getCountryId(), locations.getStreetAddress(), locations.getPostalCode(), locations.getCity(), locations.getStateProvince()));
		}	
		return ret;
	}
	
	@Transactional
	public void save(LocationsDTO locationsDTO) 
	{		
		Countries countries = countriesRepository.findById(locationsDTO.getCountryId());		
		Locations locations = new Locations(locationsDTO.getLocationId(), locationsDTO.getCity());
		locations.setCountries(countries);		
		locations.setStreetAddress(locationsDTO.getStreetAddress());
		locations.setPostalCode(locationsDTO.getPostalCode());
		locations.setStateProvince(locationsDTO.getStateProvince());	
		locationsRepository.save(locations);
	}
	
	@Transactional
	public void deleteAll() {
		locationsRepository.deleteAll();
	}

}