package com.mycompany.myapp.service.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mycompany.myapp.domain.CountriesDTO;
import com.mycompany.myapp.domain.RegionsDTO;
import com.mycompany.myapp.model.Countries;
import com.mycompany.myapp.model.Regions;
import com.mycompany.myapp.repository.RegionsRepository;
import com.mycompany.myapp.service.RegionsService;

@Service
public class RegionsServiceImpl implements RegionsService 
{
	@Resource
	private RegionsRepository regionsRepository;
	
	@Transactional
	public RegionsDTO findById(String id) {
		Regions regions = regionsRepository.findOne(new BigDecimal(id));
		return new RegionsDTO (regions.getRegionId(), regions.getRegionName());
	}
	
	@Transactional
	public List<RegionsDTO> findAll() {	
		List<RegionsDTO> ret = new ArrayList<RegionsDTO>();
		for(Regions regions : regionsRepository.findAll())
		{
			ret.add(new RegionsDTO (regions.getRegionId(), regions.getRegionName()));
			
		}
		return ret;
	}
	
	@Transactional
	public void save(RegionsDTO regionsDTO) {
		Regions regions = new Regions(regionsDTO.getRegionId());
		regions.setRegionName(regionsDTO.getRegionName());
		regionsRepository.save(regions);
	}
	
	@Transactional
	public void update(BigDecimal id, RegionsDTO regionsDTO)
	{	
		Regions regions = regionsRepository.findOne(id);
		if(regionsDTO.getRegionName() != null)
		{
			regions.setRegionName(regionsDTO.getRegionName());		
		}
		regionsRepository.save(regions);
	}
	
	@Transactional
	public void delete(BigDecimal id)
	{	
		Regions regions = regionsRepository.findOne(id);
		regionsRepository.delete(regions);
	}
	
	@Transactional
	public void deleteAll() {
		regionsRepository.deleteAll();
	}

}
