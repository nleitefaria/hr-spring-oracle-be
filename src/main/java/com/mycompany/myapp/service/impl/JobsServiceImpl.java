package com.mycompany.myapp.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mycompany.myapp.domain.JobsDTO;
import com.mycompany.myapp.model.Jobs;
import com.mycompany.myapp.repository.JobsRepository;
import com.mycompany.myapp.service.JobsService;

@Service
public class JobsServiceImpl implements JobsService 
{
	@Resource
	private JobsRepository jobsRepository;
	
	@Transactional
	public JobsDTO findById(String id) 
	{
		Jobs jobs = jobsRepository.findOne(id);
		return new JobsDTO(jobs.getJobId(), jobs.getJobTitle(), jobs.getMinSalary(), jobs.getMaxSalary());
	}
	
	@Transactional
	public List<JobsDTO> findAll() 
	{
		List<JobsDTO> ret = new ArrayList<JobsDTO>();
		for(Jobs jobs : jobsRepository.findAll())
		{
			ret.add(new JobsDTO(jobs.getJobId(), jobs.getJobTitle(), jobs.getMinSalary(), jobs.getMaxSalary()));
		}
		return ret;
	}
	
	@Transactional
	public void save(JobsDTO jobsDTO)
	{
		Jobs jobs = new Jobs(jobsDTO.getJobId(), jobsDTO.getJobTitle());
		jobs.setMinSalary(jobsDTO.getMinSalary());
		jobs.setMaxSalary(jobsDTO.getMaxSalary());
		jobsRepository.save(jobs);
	}
	
	@Transactional
	public void delete(String id)
	{
		Jobs jobs = jobsRepository.findOne(id);
		jobsRepository.delete(jobs);
	}
	
	@Transactional
	public void deleteAll()
	{
		jobsRepository.deleteAll();
	}

}
