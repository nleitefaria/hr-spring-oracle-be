package com.mycompany.myapp.service;

import java.util.List;
import com.mycompany.myapp.domain.JobsDTO;

public interface JobsService 
{
	JobsDTO findById(String id);
	List<JobsDTO> findAll();
	void save(JobsDTO jobsDTO);
	void delete(String id);
	void deleteAll();
}
