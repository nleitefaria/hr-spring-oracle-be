package com.mycompany.myapp;

import org.junit.Test;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@ComponentScan(value = "com.mycompany.myapp")
@EnableJpaRepositories(value = "com.mycompany.myapp.repository")
@EntityScan(basePackages = {"com.mycompany.myapp.model"})
@DataJpaTest
public class HrSpringOracleBeApplicationTests {

	@Test
	public void contextLoads() {
	}

}
