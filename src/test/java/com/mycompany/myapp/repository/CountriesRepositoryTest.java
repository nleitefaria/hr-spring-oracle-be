package com.mycompany.myapp.repository;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.mycompany.myapp.HrSpringOracleBeApplicationTests;
import com.mycompany.myapp.model.Countries;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = HrSpringOracleBeApplicationTests.class)
@ActiveProfiles("test")
public class CountriesRepositoryTest {
	
	@Autowired
	CountriesRepository countriesRepository;
	
	@Test
    public void findAllCountriesTest()
    {    	
		Countries c = new Countries("A");
		countriesRepository.save(c);       
        assertEquals(1, countriesRepository.findAll().size());
        
        countriesRepository.deleteAll();
    }
	
	@Test
    public void saveCountriesTest()
    {    	
		Countries c = new Countries("A");
		countriesRepository.save(c);       
        assertEquals(1, countriesRepository.findAll().size());
        
        countriesRepository.deleteAll();
    }
	



}
