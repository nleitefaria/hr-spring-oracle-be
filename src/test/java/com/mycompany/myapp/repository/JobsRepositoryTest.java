package com.mycompany.myapp.repository;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.mycompany.myapp.HrSpringOracleBeApplicationTests;
import com.mycompany.myapp.model.Jobs;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = HrSpringOracleBeApplicationTests.class)
@ActiveProfiles("test")
public class JobsRepositoryTest 
{
	@Autowired
	JobsRepository jobsRepository;
	
	@Test
    public void findAOneJobsTest()
    {    	
		Jobs jobs = new Jobs("1", "Job1");
		jobsRepository.save(jobs);
		assertEquals(1, jobsRepository.findAll().size()); 
		assertEquals("1", jobsRepository.findOne("1").getJobId()); 
		assertEquals("Job1", jobsRepository.findOne("1").getJobTitle()); 
		jobsRepository.deleteAll();
    }
	
	@Test
    public void findAllJobsTest()
    {    	
		Jobs jobs = new Jobs("1", "Job1");
		jobsRepository.save(jobs);
		assertEquals(1, jobsRepository.findAll().size());    
		jobsRepository.deleteAll();
    }
	
	@Test
    public void saveJobsTest()
    {    	
		Jobs jobs1 = new Jobs("1", "Job1");
		jobsRepository.save(jobs1);	
		Jobs jobs2 = new Jobs("2", "Job2");
		jobsRepository.save(jobs2);	
		assertEquals(2, jobsRepository.findAll().size());    
		jobsRepository.deleteAll();
    }
	
	@Test
    public void deleteJobsTest()
    {    	
		Jobs jobs = new Jobs("1", "Job1");
		jobsRepository.save(jobs);
		jobsRepository.delete(jobs);
		assertEquals(0, jobsRepository.findAll().size());    
		jobsRepository.deleteAll();
    }
}
