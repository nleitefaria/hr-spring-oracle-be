package com.mycompany.myapp.repository;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.mycompany.myapp.HrSpringOracleBeApplicationTests;
import com.mycompany.myapp.model.Locations;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = HrSpringOracleBeApplicationTests.class)
@ActiveProfiles("test")
public class LocationsRepositoryTest {
	
	@Autowired
	LocationsRepository locationsRepository;
	
	@Test
    public void findAllLocationsTest()
    {    	
		Locations l = new Locations((short) 1, "Oporto");
		locationsRepository.save(l);       
        assertEquals(1, locationsRepository.findAll().size());
        
        locationsRepository.deleteAll();
    }
	
	@Test
    public void saveLocationsTest()
    {    	
		Locations l = new Locations((short) 1, "Oporto");
		locationsRepository.save(l);       
        assertEquals(1, locationsRepository.findAll().size());
        
        locationsRepository.deleteAll();
    }

}
