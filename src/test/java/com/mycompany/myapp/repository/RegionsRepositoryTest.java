package com.mycompany.myapp.repository;

import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.mycompany.myapp.HrSpringOracleBeApplicationTests;
import com.mycompany.myapp.model.Regions;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = HrSpringOracleBeApplicationTests.class)
@ActiveProfiles("test")
public class RegionsRepositoryTest {
	
	@Autowired
	RegionsRepository regionsRepository;
	
	@Test
    public void findAllRegionsTest()
    {    	
		Regions r = new Regions(new BigDecimal(1));
		regionsRepository.save(r);       
        assertEquals(1, regionsRepository.findAll().size());
        
        regionsRepository.deleteAll();
    }
	
	@Test
    public void saveRegionsTest()
    {    	
		Regions r = new Regions(new BigDecimal(1));
		regionsRepository.save(r);       
        assertEquals(1, regionsRepository.findAll().size());
        
        regionsRepository.deleteAll();
    }

}
