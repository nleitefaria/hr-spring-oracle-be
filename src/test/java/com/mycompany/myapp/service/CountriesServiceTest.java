package com.mycompany.myapp.service;

import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.mycompany.myapp.HrSpringOracleBeApplicationTests;
import com.mycompany.myapp.domain.CountriesDTO;
import com.mycompany.myapp.domain.RegionsDTO;
import com.mycompany.myapp.model.Countries;
import com.mycompany.myapp.model.Regions;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = HrSpringOracleBeApplicationTests.class)
@ActiveProfiles("test")
public class CountriesServiceTest {
	
	@Autowired
	CountriesService countriesService;
	
	@Autowired
	RegionsService regionsService;
	
	@Test
    public void findAllCountriesTest()
    {
		RegionsDTO rDTO = new RegionsDTO(new BigDecimal(1), "Europe");
		regionsService.save(rDTO);       
		CountriesDTO cDTO = new CountriesDTO("A", "A", new BigDecimal(1));
		countriesService.save(cDTO);       
        assertEquals(1, countriesService.findAllCountries().size());
        
        countriesService.deleteAll();
        regionsService.deleteAll();
    }
	
	@Test
    public void saveCountriesTest()
    {    	
		RegionsDTO rDTO = new RegionsDTO(new BigDecimal(1), "Europe");
		regionsService.save(rDTO);       
		CountriesDTO cDTO = new CountriesDTO("A", "A", new BigDecimal(1));
		countriesService.save(cDTO);       
        assertEquals(1, countriesService.findAllCountries().size());
        
        countriesService.deleteAll();
        regionsService.deleteAll();
    }

}
