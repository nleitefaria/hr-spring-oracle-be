package com.mycompany.myapp.service;

import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.mycompany.myapp.HrSpringOracleBeApplicationTests;
import com.mycompany.myapp.domain.RegionsDTO;
import com.mycompany.myapp.model.Regions;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = HrSpringOracleBeApplicationTests.class)
@ActiveProfiles("test")
public class RegionsServiceTest {
	
	@Autowired
	RegionsService regionsService;
	
	@Test
    public void findAllRegionsTest()
    {    	
		RegionsDTO rDTO = new RegionsDTO(new BigDecimal(1), "Europe");
		regionsService.save(rDTO);       
        assertEquals(1, regionsService.findAll().size());
        
        regionsService.deleteAll();
    }
	
	@Test
    public void saveRegionsTest()
    {    	
		RegionsDTO rDTO = new RegionsDTO(new BigDecimal(1), "Europe");
		regionsService.save(rDTO);         
        assertEquals(1, regionsService.findAll().size());
        
        regionsService.deleteAll();
    }

}
