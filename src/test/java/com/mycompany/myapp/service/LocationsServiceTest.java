package com.mycompany.myapp.service;

import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.mycompany.myapp.HrSpringOracleBeApplicationTests;
import com.mycompany.myapp.domain.CountriesDTO;
import com.mycompany.myapp.domain.LocationsDTO;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = HrSpringOracleBeApplicationTests.class)
@ActiveProfiles("test")
public class LocationsServiceTest {
	
	@Autowired
	LocationsService locationsService;
	
	@Autowired
	CountriesService countriesService;
	
	@Autowired
	RegionsService regionsService;
	
	@Test
    public void findAllLocationsTest()
    {     
		CountriesDTO cDTO = new CountriesDTO("A", "A", new BigDecimal(1));
		countriesService.save(cDTO); 		
		LocationsDTO lDTO = new LocationsDTO((short) 1, cDTO.getCountryId(), "Addr1", "4150-063", "Oporto", "Oporto");
		locationsService.save(lDTO); 
	
        assertEquals(1, locationsService.findAll().size());
        
        
        locationsService.deleteAll();
        countriesService.deleteAll();
    }
	
	@Test
    public void saveLocationsTest()
    {    	
		CountriesDTO cDTO = new CountriesDTO("A", "A", new BigDecimal(1));
		countriesService.save(cDTO); 		
		LocationsDTO lDTO = new LocationsDTO((short) 1, cDTO.getCountryId(), "Addr1", "4150-063", "Oporto", "Oporto");
		locationsService.save(lDTO); 
	
        assertEquals(1, locationsService.findAll().size());
        
        
        locationsService.deleteAll();
        countriesService.deleteAll();
    }

}
