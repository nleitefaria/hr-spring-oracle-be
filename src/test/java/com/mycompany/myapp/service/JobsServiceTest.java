package com.mycompany.myapp.service;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.mycompany.myapp.HrSpringOracleBeApplicationTests;
import com.mycompany.myapp.domain.JobsDTO;
import com.mycompany.myapp.model.Jobs;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = HrSpringOracleBeApplicationTests.class)
@ActiveProfiles("test")
public class JobsServiceTest {
	
	@Autowired
	JobsService jobsService;
	
	@Test
    public void findAOneJobsTest()
    {    	
		JobsDTO jobsDTO = new JobsDTO("1", "Job1", null, null);
		jobsService.save(jobsDTO);
		assertEquals(1, jobsService.findAll().size()); 
		assertEquals("1", jobsService.findById("1").getJobId()); 
		assertEquals("Job1", jobsService.findById("1").getJobTitle()); 
		jobsService.deleteAll();
    }
	
	@Test
    public void findAllJobsTest()
    {    	
		JobsDTO jobsDTO = new JobsDTO("1", "Job1", null, null);
		jobsService.save(jobsDTO);
		assertEquals(1, jobsService.findAll().size());    
		jobsService.deleteAll();
    }
	
	@Test
    public void saveJobsTest()
    {    	
		JobsDTO jobsDTO = new JobsDTO("1", "Job1", null, null);
		jobsService.save(jobsDTO);
		assertEquals(1, jobsService.findAll().size());    
		jobsService.deleteAll();
    }
	
	@Test
    public void deleteJobsTest()
    {    	
		JobsDTO jobsDTO = new JobsDTO("1", "Job1", null, null);
		jobsService.save(jobsDTO);
		jobsService.delete("1");
		assertEquals(0, jobsService.findAll().size());    
		jobsService.deleteAll();
    }

}
