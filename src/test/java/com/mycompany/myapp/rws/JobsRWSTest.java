package com.mycompany.myapp.rws;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.hamcrest.Matchers;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.mycompany.myapp.HrSpringOracleBeApplicationTests;
import com.mycompany.myapp.domain.JobsDTO;
import com.mycompany.myapp.model.Departments;
import com.mycompany.myapp.model.Jobs;
import com.mycompany.myapp.service.DepartmentsService;
import com.mycompany.myapp.service.JobsService;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = HrSpringOracleBeApplicationTests.class)
@ActiveProfiles("test")
@AutoConfigureMockMvc
public class JobsRWSTest {
	
	@Autowired
    private MockMvc mockMvc;
	
	@Autowired
	JobsService jobsService;
	
	@Test
    public void getAllJobsTest() throws Exception 
	{  	
		JobsDTO jobsDTO = new JobsDTO("1", "Job1", null, null);
		jobsService.save(jobsDTO);
    	
        this.mockMvc.perform(get("/jobs")).andExpect(status().isOk())
        									.andExpect(content().contentType("application/json;charset=UTF-8"))
        									.andExpect(jsonPath("$", Matchers.hasSize(1))); 
        
        jobsService.deleteAll();
    }


}
