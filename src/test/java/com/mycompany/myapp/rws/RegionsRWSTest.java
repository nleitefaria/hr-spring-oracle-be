package com.mycompany.myapp.rws;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.math.BigDecimal;

import org.hamcrest.Matchers;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.mycompany.myapp.HrSpringOracleBeApplicationTests;
import com.mycompany.myapp.domain.RegionsDTO;
import com.mycompany.myapp.model.Regions;
import com.mycompany.myapp.service.RegionsService;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = HrSpringOracleBeApplicationTests.class)
@ActiveProfiles("test")
@AutoConfigureMockMvc
public class RegionsRWSTest {
	
	@Autowired
    private MockMvc mockMvc;
	
	@Autowired
	RegionsService regionsService;

    @Test
    public void getAllRegionsTest() throws Exception {
    	
    	RegionsDTO rDTO = new RegionsDTO(new BigDecimal(1), "Europe");
		regionsService.save(rDTO);         
    	
        this.mockMvc.perform(get("/regions")).andExpect(status().isOk())
        									.andExpect(content().contentType("application/json;charset=UTF-8"))
        									.andExpect(jsonPath("$", Matchers.hasSize(1))); 
        
        regionsService.deleteAll();
    }

}
